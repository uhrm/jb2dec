
import jb2dec as jb2

from PIL import Image, ImageFile


class Jbig2Decoder(ImageFile.PyDecoder):

    name = 'jbig2'

    def init(self, args):
        self.page = args[0]

    def decode(self, buf):
        self.set_as_raw(self.page.data, ('1', self.page.stride, 1))
        return -1, 0

Image.register_decoder(Jbig2Decoder.name, Jbig2Decoder)


class Jbig2ImageFile(ImageFile.ImageFile):

    format = 'JBIG2'
    format_description = 'JBIG2 bi-level image format'

    def _open(self):

        # load JBIG2 data
        buf = self.fp.read()
        ctx = jb2.Jbig2Ctx()
        ctx.data_in(buf, len(buf))
        page = ctx.page_out()

        # image data
        self._size = (page.width, page.height)
        self.mode = '1'
        self.tile = [
            (Jbig2Decoder.name, (0, 0, page.width, page.height), 0, (page,))
        ]

Image.register_open(Jbig2ImageFile.format, Jbig2ImageFile)
Image.register_extension(Jbig2ImageFile.format, ".jb2")
