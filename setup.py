import distutils.sysconfig
import setuptools
import shutil
import sys
import os

from distutils.core import setup, Extension


setup(
    name = 'jb2dec',
    version = '0.2.0',
    description = 'Python wrapper module for jbig2dec',
    author = 'Markus Uhr',
    author_email = 'uhrmar@gmail.com',
    license = 'GPLv3',
    url = 'https://gitlab.com/uhrm/jb2dec/',
    py_modules = ['jb2pil'],
    ext_modules = [
        Extension(
            "jb2dec",
            sources = ["jb2dec.c"],
            define_macros = [("HAVE_CONFIG_H", None)],
            # extra_compile_args=['-g', '-Og'],
            libraries = ['jbig2dec']
        ),
    ],
)

