#define PY_SSIZE_T_CLEAN
#include <Python.h>
#include <structmember.h>

#include <jbig2.h>

#define JB2DEC_MODULE "jb2dec"


typedef struct {
  PyObject_HEAD
  Jbig2Ctx *ctx;
  PyObject *error_callback;
} Jbig2CtxObject;


typedef struct {
  PyObject_HEAD
  Jbig2CtxObject *owner;
  Jbig2Image *img;
} Jbig2ImageObject;


/*
 * Jbig2ImageObject implementation
 */

static void
Jbig2Image_dealloc(PyObject *self)
{
  // fprintf(stderr, "*** : Free jbig2dec image: %p (ref count: %d)\n", ((Jbig2ImageObject*)self)->img, ((Jbig2ImageObject*)self)->img->refcount);
  Jbig2CtxObject *owner = ((Jbig2ImageObject*)self)->owner;
  jbig2_release_page(owner->ctx, ((Jbig2ImageObject*)self)->img);
  Py_DECREF(owner);
  Py_TYPE(self)->tp_free(self);
}


static PyObject *
Jbig2Image_get_width(PyObject *self, void *closure)
{
  return PyLong_FromLong(((Jbig2ImageObject*)self)->img->width);
}


static PyObject *
Jbig2Image_get_height(PyObject *self, void *closure)
{
  return PyLong_FromLong(((Jbig2ImageObject*)self)->img->height);
}


static PyObject *
Jbig2Image_get_stride(PyObject *self, void *closure)
{
  return PyLong_FromLong(((Jbig2ImageObject*)self)->img->stride);
}


static PyObject *
Jbig2Image_get_data(PyObject *self, void *closure)
{
  Jbig2Image *img = ((Jbig2ImageObject*)self)->img;
  return PyBytes_FromStringAndSize((char *)img->data, (img->height-1)*img->stride + ((img->width+7)>>3));
}


// static PyMethodDef Jbig2Image_methods[] = {
//   { NULL }
// };


static PyMemberDef Jbig2Image_members[] = {
  { "owner", T_OBJECT, offsetof(Jbig2ImageObject, owner), READONLY,
    "The owning JBIG2 context" },
  { NULL }
};


static PyGetSetDef Jbig2Image_properties[] = {
  { "width", Jbig2Image_get_width, NULL,
    "The image width", NULL },
  { "height", Jbig2Image_get_height, NULL,
    "The image height", NULL },
  { "stride", Jbig2Image_get_stride, NULL,
    "The image row size in bytes", NULL },
  { "data", Jbig2Image_get_data, NULL,
    "The raw bytes of the decoded image", NULL },
  { NULL }
};


static PyTypeObject Jbig2ImageType = {
  PyObject_HEAD_INIT(NULL)
  .tp_name = JB2DEC_MODULE ".Jbig2Image",
  .tp_basicsize = sizeof(Jbig2ImageObject),
  .tp_dealloc = Jbig2Image_dealloc,
  .tp_flags = Py_TPFLAGS_DEFAULT,
  .tp_doc = JB2DEC_MODULE ".Jbig2Image decoded image object",
  // .tp_methods = Jbig2Image_methods,
  .tp_members = Jbig2Image_members,
  .tp_getset = Jbig2Image_properties,
};


/*
 * Jbig2CtxObject implementation
 */

static void
Jbig2Ctx_dealloc(PyObject *self)
{
  // fprintf(stderr, "*** : Free jbig2dec ctx: %p\n", ((Jbig2CtxObject*)self)->ctx);
  jbig2_ctx_free(((Jbig2CtxObject*)self)->ctx);
  if (((Jbig2CtxObject*)self)->error_callback) {
    Py_DECREF(((Jbig2CtxObject*)self)->error_callback);
  }
  Py_TYPE(self)->tp_free(self);
}


static void
jbig2dec_callback_wrapper(void *data, const char *msg, Jbig2Severity severity, int32_t seg_idx)
{
  PyObject_CallFunction((PyObject *)data, "sii", msg, severity, seg_idx);
}

static PyObject *
Jbig2Ctx_new(PyTypeObject *type, PyObject *args, PyObject *kwds)
{
  Jbig2CtxObject *self = (Jbig2CtxObject*)type->tp_alloc(type, 0);
  if (self == NULL) {
    return NULL;
  }

  static char *kwargs[] = {
    "embedded",
    "error_callback",
    NULL
  };
  int embedded = 0;
  if (!PyArg_ParseTupleAndKeywords(args, kwds, "|$iO", kwargs, &embedded, &self->error_callback)) {
    PyErr_SetString(PyExc_ValueError, "Error parsing input arguments");
    return NULL;
  }
  if (self->error_callback) {
    Py_INCREF(self->error_callback);
  }

  int options = 0;
  if (embedded) {
    options |= JBIG2_OPTIONS_EMBEDDED;
  }
  self->ctx = jbig2_ctx_new(NULL, options, NULL, self->error_callback ? jbig2dec_callback_wrapper : NULL, self->error_callback);
  // fprintf(stderr, "*** : New jbig2dec ctx: %p\n", self->ctx);
  return (PyObject*)self;
}


static PyObject *
Jbig2Ctx_data_in(Jbig2CtxObject *self, PyObject *args)
{
  Py_buffer buf;
  int size;
  if (!PyArg_ParseTuple(args, "y*i", &buf, &size)) {
    PyErr_SetString(PyExc_ValueError, "Error parsing input arguments");
    return NULL;
  }

  // note: we cannot release the GIL here because of the error callback
  int err = jbig2_data_in(self->ctx, buf.buf, size);
  if (err != 0) {
    PyErr_SetString(PyExc_SyntaxError, "Invalid JBIG2 data stream");
    return NULL;
  }
  Py_RETURN_NONE;
}


static PyObject *
Jbig2Ctx_page_out(Jbig2CtxObject *self)
{
  // note: we cannot release the GIL here because of the error callback
  Jbig2Ctx *ctx = self->ctx;
  Jbig2Image *img = jbig2_page_out(ctx);
  if (img == NULL) {
    jbig2_complete_page(ctx);
    img = jbig2_page_out(ctx);
  }
  if (img == NULL) {
    Py_RETURN_NONE;
  }
  // fprintf(stderr, "*** : New jbig2dec image: %p (ref count: %d)\n", self->ctx, img->refcount);
  // see also https://stackoverflow.com/questions/4163018/create-an-object-using-pythons-c-api
  Jbig2ImageObject *page = PyObject_New(Jbig2ImageObject, (PyTypeObject *)&Jbig2ImageType);
  if (page == NULL) {
    jbig2_release_page(ctx, img);
    PyErr_SetString(PyExc_ValueError, "Failed to allocate image object");
    return NULL;
  }
  // page = (Jbig2ImageObject *)PyObject_Init((PyObject *)page, (PyTypeObject *)&Jbig2ImageType);
  // if (page == NULL) {
  //   jbig2_release_page(ctx, img);
  //   PyErr_SetString(PyExc_ValueError, "Failed to initialize image object");
  //   return NULL;
  // }
  Py_INCREF(self);
  page->owner = self;
  page->img = img;
  return (PyObject *)page;
}


static PyMethodDef Jbig2Ctx_methods[] = {
  {"data_in", (PyCFunction)Jbig2Ctx_data_in, METH_VARARGS,
   "Set JBIG2 input data stream for decoding."},
  {"page_out", (PyCFunction)Jbig2Ctx_page_out, METH_NOARGS,
   "Get the next available JBIG2 page image."},
  { NULL }
};


// static PyMemberDef Jbig2Ctx_members[] = {
//   { NULL }
// };


static PyTypeObject Jbig2CtxType = {
  PyObject_HEAD_INIT(NULL)
  .tp_name = JB2DEC_MODULE ".Jbig2Ctx",
  .tp_basicsize = sizeof(Jbig2CtxObject),
  .tp_dealloc = Jbig2Ctx_dealloc,
  .tp_flags = Py_TPFLAGS_DEFAULT,
  .tp_doc = JB2DEC_MODULE ".Jbig2Ctx decoder context object",
  .tp_methods = Jbig2Ctx_methods,
  // .tp_members = Jbig2Ctx_members,
  .tp_new = Jbig2Ctx_new,
};


/*
 * module initialization
 */

static struct PyModuleDef moduledef = {
  PyModuleDef_HEAD_INIT,
  .m_name = JB2DEC_MODULE,
  .m_doc = "Python module for jbig2dec library",
  .m_size = -1,
};


PyMODINIT_FUNC
PyInit_jb2dec(void)
{
  if (PyType_Ready(&Jbig2ImageType) < 0) {
    return NULL;
  }
  if (PyType_Ready(&Jbig2CtxType) < 0) {
    return NULL;
  }

  PyObject *m = PyModule_Create(&moduledef);
  if (m == NULL) {
    return NULL;
  }

  Py_INCREF(&Jbig2ImageType);
  if (PyModule_AddObject(m, "Jbig2Image", (PyObject*)&Jbig2ImageType) < 0) {
    return NULL;
  }

  Py_INCREF(&Jbig2CtxType);
  if (PyModule_AddObject(m, "Jbig2Ctx", (PyObject*)&Jbig2CtxType) < 0) {
    return NULL;
  }

  if (PyModule_AddIntConstant(m, "SEVERITY_DEBUG", JBIG2_SEVERITY_DEBUG) < 0) {
    return NULL;
  }
  if (PyModule_AddIntConstant(m, "SEVERITY_INFO", JBIG2_SEVERITY_INFO) < 0) {
    return NULL;
  }
  if (PyModule_AddIntConstant(m, "SEVERITY_WARNING", JBIG2_SEVERITY_WARNING) < 0) {
    return NULL;
  }
  if (PyModule_AddIntConstant(m, "SEVERITY_FATAL", JBIG2_SEVERITY_FATAL) < 0) {
    return NULL;
  }


  return m;
}

